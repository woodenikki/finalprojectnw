import { Component, HostListener, ViewChild, ElementRef } from '@angular/core';
//https://codepen.io/gabrielcarol/pen/rGeEbY

import { SongService } from './song.service';
import { TechService } from './tech.service';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  @ViewChild('inputMessage', { static: true }) public inputMessage: ElementRef<HTMLInputElement>;

  title = 'pianoApp';
  public keyDictionary = {};
  song$;
  tech$;
  public currentSong: string;
  public currentSongIndex: number;
  public songList = {};

  //https://egghead.io/lessons/angular-fetch-data-from-an-api-using-the-httpclient-in-angular
  constructor(private songService: SongService, private techService: TechService){
    this.currentSong = "assets/SongFiles/KissTheRain.txt";
    this.currentSongIndex = 0;

    this.keyDictionary["z"] = "1C";
    this.keyDictionary["x"] = "1D";
    this.keyDictionary["c"] = "1E";
    this.keyDictionary["v"] = "1F";
    this.keyDictionary["b"] = "1G";
    this.keyDictionary["n"] = "2A";
    this.keyDictionary["m"] = "2B";
    this.keyDictionary[","] = "2C";
    this.keyDictionary["."] = "2D";
    this.keyDictionary["/"] = "2E";
    this.keyDictionary["a"] = "2F";
    this.keyDictionary["s"] = "2G";
    this.keyDictionary["d"] = "3A";
    this.keyDictionary["f"] = "3B";
    this.keyDictionary["g"] = "3C";
    this.keyDictionary["h"] = "3D";
    this.keyDictionary["j"] = "3E";
    this.keyDictionary["k"] = "3F";
    this.keyDictionary["l"] = "3G";
    this.keyDictionary[";"] = "4A";
    this.keyDictionary["q"] = "4B";
    this.keyDictionary["w"] = "4C";
    this.keyDictionary["e"] = "4D";
    this.keyDictionary["r"] = "4E";
    this.keyDictionary["t"] = "4F";
    this.keyDictionary["y"] = "4G";
    this.keyDictionary["u"] = "5A";
    this.keyDictionary["i"] = "5B";
    this.keyDictionary["o"] = "5C";
    this.keyDictionary["p"] = "5D";
    this.keyDictionary["2"] = "5E";
    this.keyDictionary["3"] = "5F";
    this.keyDictionary["4"] = "5G";
    this.keyDictionary["7"] = "6A";
    this.keyDictionary["8"] = "6B";
    this.keyDictionary["9"] = "6C";
    //white ^ / black v
    this.keyDictionary["Z"] = "1Ca";
    this.keyDictionary["X"] = "1Da";
    this.keyDictionary["V"] = "1Fa";
    this.keyDictionary["B"] = "1Ga";
    this.keyDictionary["N"] = "2Aa";
    this.keyDictionary["<"] = "2Ca";
    this.keyDictionary[">"] = "2Da";
    this.keyDictionary["A"] = "2Fa";
    this.keyDictionary["S"] = "2Ga";
    this.keyDictionary["D"] = "3Aa";
    this.keyDictionary["G"] = "3Ca";
    this.keyDictionary["H"] = "3Da";
    this.keyDictionary["K"] = "3Fa";
    this.keyDictionary["L"] = "3Ga";
    this.keyDictionary[":"] = "4Aa";
    this.keyDictionary["W"] = "4Ca";
    this.keyDictionary["E"] = "4Da";
    this.keyDictionary["T"] = "4Fa";
    this.keyDictionary["Y"] = "4Ga";
    this.keyDictionary["U"] = "5Aa";
    this.keyDictionary["O"] = "5Ca";
    this.keyDictionary["P"] = "5Da";
    this.keyDictionary["#"] = "5Fa";
    this.keyDictionary["$"] = "5Ga";
    this.keyDictionary["&"] = "6Aa";

    //songlist
    this.songList[0] = "assets/SongFiles/KissTheRain.txt";
    this.songList[1] = "assets/SongFiles/iGiorni.txt";
    this.songList[2] = "assets/SongFiles/mary.txt";
    this.songList[3] = "assets/SongFiles/moonlight.txt";
    this.songList[4] = "assets/SongFiles/hallelujah.txt";
    this.songList[5] = "assets/SongFiles/furElise.txt";
    this.songList[6] = "assets/SongFiles/crisis.txt";
  }


  fetchSong(){
    this.song$ = this.songService.fetchSong();
  }

  fetchTech(){
    this.tech$ = this.techService.fetchTech();
  }

  public changeIndex(index: number){
    this.currentSongIndex = index;
  }

  public changeSong(file: string){
    this.currentSong = file;
    document.getElementById("sheetMusic").setAttribute('data', this.currentSong);
  }

  public currentSongFile(): string{
    return this.currentSong;
  }
  public nextSong(index: number){
    if (index === 6){
      index = 0;
    } else{
      index++;
    }
    this.currentSongIndex = index;
    this.currentSong = this.songList[this.currentSongIndex];
    document.getElementById("sheetMusic").setAttribute('data', this.currentSong);
  }

  public inputMessageClick() {
    this.inputMessage.nativeElement.focus();
  }
  
  //from https://stackoverflow.com/questions/35527456/angular-window-resize-event
 

  @HostListener('window:mousedown', ['$event'])
  public clickNote(e: MouseEvent) { //deleted ,noteName: string
    //const audio = <HTMLElement> document.querySelector(`audio[data-key="${e.keyCode}"]`) as HTMLAudioElement;

    let element = document.getElementById((e.target as HTMLElement).id);
    let myClass: string = (e.target as HTMLElement).className;
    let myDiv = (e.target as HTMLElement);
    myDiv.classList.add("active")

    if(myClass === 'w' || myClass ==='b'){
      let audio = new Audio();
  
      let myFileBag: string;
  
      if(myClass === 'w'){
        myFileBag = "assets/Notes/White/";
      }else if(myClass === 'b'){
        myFileBag = "assets/Notes/Black/";
      }
      
  
      //let myFileBeg: string = "assets/Notes/White/";
      let myFile: string = (e.target as HTMLElement).id;
      let myFileEnd: string = ".mp3";
  
      let myFullFile: string = (myFileBag.concat(myFile)).concat(myFileEnd); //myFile was noteName
      audio.src = myFullFile;
      //audio.src = "assets/Notes/White/1C.mp3";
      audio.load();
      audio.play();
      audio.currentTime = 0;
    }
    console.log(element);
  }

  
  @HostListener('window:mouseup', ['$event.target'])
  public onMouseUp(target: HTMLElement) {
    target.classList.remove("active");
  }


  @HostListener('window:keydown', ['$event'])
  public playNote(e: KeyboardEvent) {
    try{
      //make .b or .w class active on key down..?
      let myClass: string = (e.target as HTMLElement).className;
      let keyPressed: string = e.key;

      const audio = document.getElementById(keyPressed) as HTMLAudioElement;


      let div = (document.getElementById(this.keyDictionary[keyPressed]) as HTMLDivElement);
        div.classList.add("active");
      
      audio.currentTime = 0;
      audio.play();
    }catch(e){ }
  }

  @HostListener('window:keyup', ['$event'])
  public onKeyUp(event: KeyboardEvent) {     
    let keyPressed: string = event.key; 
    let div = (document.getElementById(this.keyDictionary[keyPressed]) as HTMLDivElement);
    div.classList.remove("active");
  } 


}