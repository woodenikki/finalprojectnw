import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import {Observable, Subject, of, from} from 'rxjs';
//weird error from importing observable: 
    //https://stackoverflow.com/questions/51128911/error-ts2339-property-takeuntil-does-not-exist-on-type-observablefoo-and

@Injectable()
export class TechService{

    constructor(private http: HttpClient) {}

    fetchTech(): Observable<Object> {
        return this.http.get('/assets/data/techs.json');
        
    }
}