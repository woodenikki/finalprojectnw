import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import {Observable, Subject, of, from} from 'rxjs';

@Injectable()
export class SongService{

    constructor(private http: HttpClient) {}

    fetchSong(): Observable<Object> {
        return this.http.get('/assets/data/songs.json');
        
    }
}